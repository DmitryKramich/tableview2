import UIKit

class ViewController: UIViewController {
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var changedLabel: UILabel!
    
    var arrayColor = [UIColor]()
    var arrayNameColor = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.isEditing = true
        initialSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    func initialSetup() {
        arrayColor = ManagerSell.shared.getColor()
        arrayNameColor = ManagerSell.shared.getNameColor()
        tableView.reloadData()
    }
    
    @IBAction func colorButtonPressed(_ sender: UIButton) {
        changedLabel.text = arrayNameColor[sender.tag] //sender.tag позволяет узнать номер ячейки в которой кнопка
        changedLabel.backgroundColor = arrayColor[sender.tag]
    }
    
}

extension ViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayColor.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "colorCell", for: indexPath) as? ColorCell else {
            return UITableViewCell()
            }
            cell.backgroundColor = arrayColor[indexPath.row]
            cell.colorButton.setTitle(arrayNameColor[indexPath.row], for: .normal)
            cell.colorButton.tag = indexPath.row
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    private func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            //self.arrayNameColor?.remove(at: indexPath.row)
            self.arrayColor.remove(at: indexPath.row)
            self.tableView.reloadData()
        }
    }
    
    func tableView(_ tableView: UITableView, shouldIndentWhileEditingRowAt indexPath: IndexPath) -> Bool {
        return false
    }
    
    func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let movedObject = self.arrayColor[sourceIndexPath.row]
        arrayColor.remove(at: sourceIndexPath.row)
        arrayColor.insert(movedObject, at: destinationIndexPath.row)
        
        let movedObjectName = self.arrayNameColor[sourceIndexPath.row]
        arrayNameColor.remove(at: sourceIndexPath.row)
        arrayNameColor.insert(movedObjectName, at: destinationIndexPath.row)
        self.tableView.reloadData()
    }
    
    
}


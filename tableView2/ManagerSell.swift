import UIKit

class ManagerSell: NSObject {
    public static let shared  = ManagerSell()
    private override init() {}
    
    private let arrayColor: [UIColor] = [
        .green,
        .gray,
        .orange,
        .blue,
        .black
        ]
    private let arrayNameColor: [String] = [
        "Зеленый", "Серый", "Оранжевый", "Синий", "Черный"
    ]
    
    func getColor() -> [UIColor] {
        return arrayColor
    }
    
    func getNameColor() -> [String] {
        return arrayNameColor
    }
}
